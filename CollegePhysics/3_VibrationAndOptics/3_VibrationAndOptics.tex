\documentclass[UTF8]{ctexart}

\usepackage{NotesTex}
\usepackage{ctex}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{pgf,tikz,pgfplots}
\pgfplotsset{compat=1.15}
\usepackage{mathrsfs}
\usetikzlibrary{arrows}
\definecolor{zzttqq}{rgb}{0.6,0.2,0}
\definecolor{ududff}{rgb}{0.30196078431372547,0.30196078431372547,1}

\title{大学物理学习笔记\\[2ex]\begin{large}波动与光学篇\end{large}}

\author{wuzirui}
\emailAdd{wuzirui@tztyun.org}
\affiliation {Beijing Institute of Technology}

\begin{document}

\maketitle
\pagestyle{fancynotes}

\part{}
\section{简谐振动}
\subsection{简谐振动的表达式}

当物体运动时，如果离开平衡位置的位移(或角位移)按正余弦函数规律随时间变化，则这种运动是\textbf{简谐振动}
\begin{align}
	 & x = Acos(\omega t + \varphi)                           \\
	 & v = \frac{dx}{dt} =  -A\omega sin(\omega t + \varphi)  \\
	 & a = \frac{dv}{dt} = -A\omega^2 cos(\omega t + \varphi)
\end{align}

\begin{marginfigure}
	若已知某时刻的位移、速度$x_0,v_0$,则有
	\begin{equation}
		A = \sqrt{x_0^2 + \frac{v_0^2}{\omega^2}}
	\end{equation}
\end{marginfigure}

利用以上性质，有：
\begin{equation}
	\text{周期：}T = \frac{2\pi}{\omega}\quad\text{频率：}\nu = \frac{1}{T} = \frac{\omega}{2\pi}\quad\text{角频率：}\omega = \frac{2\pi}{T} = 2\pi\nu
\end{equation}

简谐运动系统的总能量
\begin{equation}
	E = E_p+E_k = \frac{1}{2}kx^2 + \frac{1}{2}mv^2 = \frac{1}{2}mA^2\omega^2 = \frac{1}{2}kA^2
\end{equation}
因此
\begin{equation}
	A = \sqrt{\frac{2E}{k}}
\end{equation}

对于一个周期内的简谐运动系统，我们可以计算其动能和势能的平均值
\begin{align*}
	\overline{E_k} & = \frac{1}{T}\int_0^T\frac{1}{2}m\omega^2A^2sin^2(\omega t + \varphi)dt \\
	               & =\frac{1}{4}m\omega^2A^2 = \frac{1}{4}kA^2
\end{align*}
\begin{align*}
	\overline{E_p} & = \frac{1}{T}\int_0^T\frac{1}{2}kA^2cos^2(\omega t + \varphi)dt \\
	               & = \frac{1}{4}kA^2=\frac{1}{4}m\omega^2A^2
\end{align*}
即
\begin{equation}
	\overline{E_p} = \overline{E_k}
\end{equation}

\begin{marginfigure}
	简谐运动相关的问题可以借助旋转矢量图的方式来解决
\end{marginfigure}
\begin{definition}[同相、反相、超前落后]
	对于两个周期相等的简谐振动，相位差$\Delta\varphi = \varphi_2-\varphi_1$,当：
	\begin{enumerate}
		\item $\Delta\varphi = 2k\pi$时，两个简谐振动为\textbf{同相}，即同时经过平衡位置，且同向振动;
		\item $\Delta\varphi = (2k+1)\pi$时，两个简谐振动为\textbf{反相}，即同时经过平衡位置，但反向振动;
		\item 不满足以上情况时，\textbf{将$|\Delta\varphi|$限制在$(0,\pi]$之间}，若$\Delta\varphi>0$，则第一个简谐振动\textbf{超前}，反之\textbf{落后}.
	\end{enumerate}
\end{definition}

~\\
\subsection{简谐运动方程}
对于弹性系数为k的弹簧振子，利用力学原理，有:
\begin{equation*}
	a = \frac{F}{m} = -\frac{k}{m}x:=-\omega^2x
\end{equation*}
进而有\textbf{简谐振动的微分方程}
\begin{marginfigure}
	利用三角函数与复数关系，可以把解表示成下面函数的实部:
	\begin{equation}
		\widetilde{x} = e^{i(\omega t + \varphi)}
	\end{equation}
\end{marginfigure}
\begin{equation}
	\frac{d^2x}{dt^2}+\omega^2x=0\quad \text{解的形式为}\;\; x=Acos(\omega t+\varphi)
\end{equation}
对于任意简谐振动成立.

对于弹簧振子，其做简谐振动的固有角频率、固有频率、周期分别为：
\begin{equation}
	\omega  = \sqrt{\frac{k}{m}}\quad \nu = \frac{\omega}{2\pi} = \frac{1}{2\pi}\sqrt{\frac{k}{m}}\quad T = \frac{1}{\nu} = 2\pi\sqrt{\frac{m}{k}}
\end{equation}

\begin{example}
	一质量为m的柱状容器直立浮于水中，横截面积是长为2cm宽为0.8cm的矩形。把容器稍微压低后静止释放，不计水和空气阻力，$g=10m/s^2$，则容器上下振动周期是？

	\textbf{[解]} 设平衡时容器底部距离水面 $x_0\; m$,设下压后距水面$x_1 \;m, \quad x:=x_1-x_0$,

	\begin{equation*}
		\rho gSx_0 = mg\quad \rho gSx_1 -mg = \rho gSx = m\frac{d^2x}{dt^2}
	\end{equation*}
	得到简谐振动标准微分方程,解得
	\begin{equation*}
		\omega  = \sqrt{\frac{\rho gS}{m}},\quad T=  \frac{2\pi}{\omega} = \frac{\pi}{2}
	\end{equation*}
\end{example}

~\\
\subsection{角谐运动}
以角量表示的简谐运动称为角谐运动，典型的有单摆、复摆(物理摆),其中,单摆可以视为复摆的一个特例。这里以复摆为例列写方程。

由定轴转动定律和力矩公式，有
\begin{equation*}
	M = -rmgsin\theta, \quad J\alpha = M
\end{equation*}
其中r为质心到转轴的距离,$\alpha$为角加速度,经过近似，得角谐运动的微分方程
\begin{equation}
	\frac{d^2\theta}{dt^2} + \omega^2\theta = 0\quad \omega = \sqrt{\frac{mgr}{J}}
\end{equation}
解的形式为
\begin{equation}
	\theta = \theta_0cos(\omega t + \varphi)
\end{equation}则
\begin{equation}
	T = 2\pi\sqrt{\frac{J}{mgr}},\quad\nu = \frac{1}{2\pi}\sqrt{\frac{mgr}{J}}
\end{equation}

对于单摆，可令$I = ml^2,\;r = l$,得
\begin{equation}
	\omega = \sqrt{\frac{g}{l}}\quad T = 2\pi\sqrt{\frac{l}{g}}\quad\nu = \frac{1}{2\pi}\sqrt{\frac{g}{l}}
\end{equation}

% Please add the following required packages to your document preamble:
% \usepackage{booktabs}
\begin{table}[]
	\begin{tabular}{@{}llll@{}}
		\toprule
		         & 固有角频率$\omega$     & 周期T                      & 固有频率$\nu$                        \\ \midrule
		弹簧振子 & $\sqrt{\frac{k}{m}}$   & $2\pi\sqrt{\frac{m}{k}}$   & $\frac{1}{2\pi}\sqrt{\frac{k}{m}}$   \\ \midrule
		单摆     & $\sqrt{\frac{g}{l}}$   & $2\pi\sqrt{\frac{l}{g}}$   & $\frac{1}{2\pi}\sqrt{\frac{g}{l}}$   \\ \midrule
		复摆     & $\sqrt{\frac{mgr}{J}}$ & $2\pi\sqrt{\frac{J}{mgr}}$ & $\frac{1}{2\pi}\sqrt{\frac{mgr}{J}}$ \\ \bottomrule
	\end{tabular}
\end{table}

\begin{theorem}[弹簧的串并联公式]~\\
	弹簧串联 \begin{equation}
		k_e = \frac{k_1k_2}{k_1+k_2}
	\end{equation}
	弹簧并联
	\begin{equation}
		k_e = k_1+k_2
	\end{equation}
\end{theorem}
\begin{marginfigure}
	劲度系数k的弹簧截去一半，
	\begin{equation}
		k_e = 2k
	\end{equation}
\end{marginfigure}

~\\~\\
\section{简谐振动的合成}
\subsection{同方向同频率简谐振动的合成}
设同一质点同时参与两个同方向、同频率的简谐振动，
\begin{equation*}
	x_1 = A_1cos(\omega t + \varphi_1)\quad x_2 = A_2cos(\omega t + \varphi_2)
\end{equation*}
\begin{marginfigure}
	多个同方向同频率简谐振动的合成与两个振动合成类似
\end{marginfigure}
易知,上述两个振动合成后仍是简谐振动。
\begin{equation}
	x = Acos(\omega t + \varphi)
\end{equation}
通常，可以通过在旋转矢量图中表示出两个初相的向量，通过余弦定理解出合振动的性质
\begin{marginfigure}
	一般题目中，首选用旋转矢量图解出$A,\varphi$,实在解不出再考虑使用公式
\end{marginfigure}
\begin{equation}
	A = \sqrt{A_1^2+A_2^2 + 2A_1A_2cos(\varphi_2 - \varphi_1)}
\end{equation}
\begin{equation}
	\varphi  = arctan\frac{A_1sin\varphi_1+A_2sin\varphi_2}{A_1cos\varphi_1+A_2cos\varphi_2}
\end{equation}

当两振动同相时，合振动的振幅达到最大值 $A_{max} = A_1+A_2$

当两振动反相时，合振动的振幅达到最小值 $A_{min} = |A_1 - A_2|$

因此有
\begin{equation}
	|A_1-A_2|\leq A\leq A_1+A_2
\end{equation}

~\\
\subsection{同方向不同频率的简谐振动的合成}
设同一质点同时参与两个同方向、不同频率的简谐振动，不妨让他们初相为0,振幅相等
\begin{equation*}
	x_1 = Acos(\omega_1 t)\quad x_2 = Acos(\omega_2 t)
\end{equation*}
显然他们的合振动不是简谐振动，利用和差化积公式
\begin{equation}
	x = x_1+x_2 = 2Acos(\frac{\omega_1 - \omega_2}{2}t)\cdot cos(\frac{\omega_1+\omega_2}{2}t)
\end{equation}
当两振动\textbf{频率相近}时，$\Delta\omega = |\omega_1 - \omega_2| << \omega_1+\omega_2$,合成的振动是一个\textbf{振幅做低频变换的高频振动}，振幅变化的一个周期称为一个\textbf{拍}

一秒内拍的数目称为\textbf{拍频}
\begin{equation}
	\nu = |\nu_1-\nu_2|
\end{equation}

~\\
\section{波动}
\subsection{基本概念}
\begin{definition}[横波、纵波]
	振动方向与传播方向垂直的波称为\textbf{横波}，产生横波需要介质内部有垂直与传播方向的剪切形变.因此液体和气体中不能传播机械横波

	振动方向与传播方向一致的波称为\textbf{纵波}，机械纵波具有"稠密","稀疏"区域的外形特征，需要介质局部发生压缩或膨胀.
\end{definition}

波线上相位差为$2\pi$的两点之间距离称为\textbf{波长}$\lambda$,其倒数称为\textbf{波数}$\sigma$表示单位长度所包含完整波的数目.

波数的$2\pi$倍称为角波数k
\begin{equation}
	k = 2\pi\sigma = \frac{2\pi}{\lambda}
\end{equation}
\begin{marginfigure}
	注意这里k不能表示\textbf{弹簧的劲度系数}.其单位为 $rad\cdot m^{-1}$而不是劲度系数的$N\cdot m^{-1}$
\end{marginfigure}

单位时间内，振动状态传播的距离称为\textbf{波速}$u$。
\begin{equation}
	u = \frac{\lambda}{T} = \frac{\omega}{k} = \lambda \nu
\end{equation}

\begin{marginfigure}
	常用变换
	\begin{equation}
		k = \frac{2\pi}{\lambda} = \frac{2\pi \nu}{u} = \frac{\omega}{u}
	\end{equation}
	u通常作为振动的时空特征的联系
\end{marginfigure}

\begin{remark}
	这里角波数是仿照振动时间特征描述而定义的。

	平面简谐波具有时空双重周期性，波速u为振动频率$\nu$与波长$\lambda$的乘积，类似地，记
	\textbf{波数}为单位长度出现周期的数目，则波速u可以写作
	\begin{equation}
		u=\frac{\text{路程}}{\text{时间}} = \frac{\text{周期数}}{\text{时间}}\cdot (\frac{\text{周期数}}{\text{路程}})^{-1} = \frac{\omega}{k}
	\end{equation}
	因而向右传播的简谐波的波函数写作
	\begin{align}
		y(x,t) & = Acos(\omega t- kx + \varphi_0)                         \\
		       & =Acos[2\pi(\frac{t}{T} - \frac{x}{\lambda} + \varphi_0)]
	\end{align}
	也就不足为奇了
\end{remark}
\begin{marginfigure}
	此外，k也可以理解为单位长度的\textbf{相差}，波函数中$-kx$一项也可以理解为x长度的相差
\end{marginfigure}

类似地，有周期T,频率$\nu$,角频率$\omega$
\begin{equation}
	\nu = \frac{1}{T},\quad \omega = \frac{2\pi}{T} = 2\pi\nu
\end{equation}



对于机械波而言，波速通常由介质的弹性性质、惯性性质决定

固体中
\begin{align*}
	 & u_{Shear} = \sqrt{\frac{G}{\rho}}     \\
	 & u_{Longitude} = \sqrt{\frac{E}{\rho}}
\end{align*}

\begin{marginfigure}
	G为切变模量，E为弹性模量.通常来讲，固体的弹性模量大于切变模量，因此纵波波速一般快于横波波速
\end{marginfigure}

对于气体、液体，纵波的波速
\begin{equation}
	u_L = \sqrt{\frac{K}{\rho}}
\end{equation}
K为体积模量

对于理想气体
\begin{equation}
	u_L = \sqrt{\frac{\gamma p}{\rho}} = \sqrt{\frac{\gamma RT}{M}}
\end{equation}
$\gamma$为比热容比

~\\
\subsection{简谐波}
\begin{definition}[波函数]
	表示介质中任意一点在任一时刻相对于平衡位置的位移
\end{definition}

对于向右传播的\textbf{行波},给定波形状的函数，对于其中任意一点x，它在$t=t_0$的位置等于位置$x-ut_0$在$t=0$时刻的位置，即
\begin{equation}
	y = f(x-ut), \quad u > 0
\end{equation}

即将x用$x-ut$代入

类似地，x在t时刻的位置与$x=0$在$t-\frac{x}{u}$时刻的位移相等
\begin{equation}
	y = f(t - \frac{x}{u}), \quad u>0
\end{equation}

即将t用$t-\frac{x}{u}$代入

~\\
这即是波函数的两种形式(向左传播的行波只需改变括号中的正负号)

~\\
对于简谐振动
\begin{equation*}
	y_0=Acos(\omega t +\varphi_0)
\end{equation*}
将t用$t-\frac{x}{u}$代入
\begin{equation}
	y = Acos[\omega(t-\frac{x}{u})+\varphi_0]
\end{equation}

改写成其他形式，有
\begin{align*}
	y & = Acos[2\pi(\nu t-\frac{x}{\lambda})+\varphi_0]                                                      \\
	  & = Acos[2\pi(\frac{t}{T}-\frac{x}{\lambda})+\varphi_0]                                                \\
	  & = Re[\widetilde{y}] = Re[Aexp(i(\omega t -kx+\varphi_0))]                                            \\
	  & = Acos(\omega t - kx +\varphi_0) = Acos[\frac{2\pi u}{\lambda}t - \frac{2\pi}{\lambda}x + \varphi_0]
\end{align*}

~\\
\subsection{波的能量}
设细长棒沿x轴放置，质量密度为$\rho$，横截面积S，弹性模量为E，棒以波速u
沿x轴正方向传播的简谐纵波
\begin{equation*}
	y = Acos(\omega t  - \frac{\omega x}{u})
\end{equation*}

其上任一质元的振动动能和振动势能相等
\begin{equation}
	dE_k=dE_p = \frac{1}{2}\rho dV \omega^2A^2sin^2(\omega t  - \frac{\omega x}{u})
\end{equation}

能量密度
\begin{equation}
	w = \frac{dE}{dV} = \rho\omega^2A^2sin^2(\omega t  - \frac{\omega x}{u})
\end{equation}

平均能量密度
\begin{equation}
	\bar{w} =  \frac{1}{2}\rho\omega^2A^2
\end{equation}

平均能流（功率）
\begin{equation}
	\bar{P} = uS_\perp \bar{w} = \frac{1}{2}\rho S_\perp \omega^2A^2u
\end{equation}

平均能流密度(强度)
\begin{equation}
	I = \frac{\bar{P}}{S_\perp} = \frac{1}{2}\rho u \omega^2 A^2 u = \frac{1}{2}Z\omega^2A^2
\end{equation}
其中$Z = \rho u$称为介质的\textbf{特性阻抗}
\begin{marginfigure}
	大多数时候，光强式中比例系数并不重要，通常可以写成
	\begin{equation}
		I = A^2
	\end{equation}
	在研究多个介质时，引入折射率n
	\begin{equation}
		I = nA^2
	\end{equation}
\end{marginfigure}

~\\
\subsection{惠更斯原理、波的衍射}
\begin{theorem}[惠更斯原理]
	在波的传播过程中，\textbf{波前}上的每一点都可以看作发射子波的\textbf{波源}，在此后的任意时刻，这些子波的包迹就成为新的波面
\end{theorem}

衍射是波动所独具的特征之一，其显著与否取决于狭缝的宽度a与波长之比

\begin{theorem}[反射定律]
	入射角等于反射角
	\begin{equation}
		i = i'
	\end{equation}
\end{theorem}

\begin{theorem}[折射定律]
	入射角的正弦与折射角的正弦之比等于波在两个介质中的波速之比，比值称为介质II对介质I的折射率
	\begin{equation}
		\frac{sin\;i}{sin\;\gamma} = \frac{u_1}{u_2} = n_{21}
	\end{equation}
\end{theorem}

\begin{theorem}[半波损失]
	当光从光疏介质到光密介质界面发生反射时，会产生$\pi$的相位差
\end{theorem}

~\\~\\
\section{光的干涉}
\subsection{相干条件}
\begin{itemize}
	\item 频率相同
	\item 振动方向相同
	\item 相位差恒定
\end{itemize}

两列相干光发生干涉后的光强
\begin{equation}
	I = I_1+I_2 +2\sqrt{I_1I_2}cos\Delta\varphi
\end{equation}

当$\Delta\varphi=\pm 2k\pi,\Delta\varphi=\pm(2k+1)\pi,\;k=0,1,\cdots$时，P点光强分别达到最大、最小

~\\
\subsection{光程}
\begin{definition}[光程]单色光在介质中传播r产生的相位变化与在真空中传播nr产生的相位变化相同.
	把nr定义为光程,两列光的光程差
	\begin{equation}
		\delta = n_2r_2 - n_1r_1
	\end{equation}
	则两列光的相位差
	\begin{equation}
		\Delta \varphi = \frac{2\pi}{\lambda}\delta
	\end{equation}

\end{definition}

因此，用光程差表示某点光强最大、最小的条件，即当光程差满足$\delta = \pm k\lambda, \;k = 0, 1, \cdots $时光强最大
，$\delta = \pm(2k+1)\frac{\lambda}{2}, \; k = 1,2,\cdots$时光强最小

\begin{theorem}[透镜的等光程性]
	光通过透镜时，不会引起附加的光程差。
\end{theorem}

~\\
\subsection{分波阵面干涉——杨氏双缝实验}
对于杨氏双缝，当光程差
\begin{equation}
	\delta = \frac{d}{D}x=\pm k\lambda
\end{equation}
或用x轴上位置表示
\begin{equation}
	x = \pm k\frac{D}{d}\lambda\quad k = 0,1,\cdots
\end{equation}
时P点为明纹中心，
对应的，暗纹中心
\begin{equation}
	\delta = \frac{d}{D}x = \pm(2k-1)\frac{\lambda}{2}\quad x = \pm(2k-1)\frac{D\lambda}{2d}, \quad k = 1,2,\cdots
\end{equation}
相邻两个明纹中心的间距
\begin{equation}
	\Delta x = \frac{D}{d}\lambda
\end{equation}
\begin{marginfigure}
	可以将双缝到屏理解为波长的“放大器”，$\frac{D}{d}$就是放大系数，将不可见的干涉放大到可以观察的尺度
\end{marginfigure}

将上述结论代入光强计算公式
\begin{equation*}
	I = I_1+I_2 +2\sqrt{I_1I_2}cos\Delta\varphi = 2I_0+2I_0cos\Delta\varphi
\end{equation*}
相位差
\begin{equation}
	\Delta\varphi = \frac{2\pi}{\lambda}\delta
\end{equation}
因此光屏上任一点P的光强
\begin{equation}
	I = 4I_0cos^2[\frac{\pi(r_2-r_1)}{\lambda}] = 4I_0cos^2(\frac{\pi d}{\lambda D}x) = 4I_0cos^2(\frac{\Delta\varphi}{2})
\end{equation}

~\\
\subsubsection{干涉条纹的变动}
引起条纹变动的因素

\begin{enumerate}
	\item 光源的移动
	\item 干涉装置的结构变化
	\item 光路介质变化
\end{enumerate}

通常的讨论思路有：

\textbf{考虑固定点P，观察有多少根干涉条纹移过此点}\quad 当有N根干涉条纹移过P点时，P点处光程差的该变量
\begin{equation}
	\Delta \delta = N\lambda
\end{equation}

\textbf{跟踪某级条纹的移动方向和距离}\quad 通常研究零级条纹的移动方向和移动距离

~\\
\subsection{分振幅干涉}
\textbf{等倾干涉}\quad

等倾干涉的光程差
\begin{equation}
	\delta = 2e\sqrt{n_2^2-n_1^2sin^2\;i}+\delta'  = 2necos\;r+\delta'
\end{equation}
\begin{equation}
	\delta' = \left\{\begin{matrix}
		0,\qquad \text{从光密介质到光疏介质} \\
		\frac{\pi}{2},\qquad\text{从光疏介质到光密介质}
	\end{matrix}\right.
\end{equation}
e为薄膜的厚度.干涉条纹级数k越大，对应折射角越小，入射角越小
\begin{equation}
	\Delta r = \frac{-\lambda}{2nesin\;r}
\end{equation}
当薄膜厚度改变时，可依据光程差不变的点的移动来判断。若e增加，则须有$cos\;r$减小，r增大，相当于干涉条纹一个一个从中心冒出来

\textbf{增透膜、增反膜}\quad \begin{equation}
	\delta = 2ne
\end{equation}
令其满相长、相消条件即可达到增透膜、增反膜的效果

对于增透膜
\begin{equation}
	e = \frac{(2k+1)\lambda}{4n}
\end{equation}

增反膜
\begin{equation}
	e = \frac{k\lambda}{2n}
\end{equation}

\textbf{等厚干涉}
\begin{equation}
	\delta =  2ne +\delta'
\end{equation}

薄膜厚的地方，光程差较大，级次高，相邻两个亮纹之间厚度差
\begin{equation}
	\Delta e = \frac{\lambda}{2n}
\end{equation}

相邻亮纹间距
\begin{equation}
	L = \frac{\lambda}{2nsin\theta} = \approx \frac{\lambda}{2n\theta}
\end{equation}

\textbf{牛顿环}
\begin{equation}
	\delta = 2e + \frac{\lambda}{2}
\end{equation}
\begin{equation}
	r_\text{明} = \sqrt{\frac{(2k-1)R\lambda}{2}}\quad r_\text{暗} = \sqrt{kR\lambda}
\end{equation}
\end{document}
