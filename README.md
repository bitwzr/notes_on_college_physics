# 大学物理学习笔记

#### 介绍
大学物理学习过程中掌握的技巧方法与易错知识点汇总

这本笔记采用 LaTeX 编辑，使用 [NotesTex](https://github.com/Adhumunt/NotesTeX) 模板. 插图部分使用 Tikz 作为主要图形语言

### 如何使用
打开 CollegePhysics 文件夹中各子目录中的pdf文件即可

### 如何参与
1.下载并安装 TexLive

 [官方网站](http://tug.org/texlive/)

2.将项目clone到本地，打开子目录中.tex文件进行编译

3.编辑并通过 pull request 上传